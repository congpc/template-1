import React from 'react';
import {StyleSheet, View, Dimensions, ScrollView} from 'react-native';
import {Input, Button, Icon, ListItem} from 'react-native-elements';

const ScreenWidth = Dimensions.get('window').width;

const list = new Array(10).fill({
  name: 'Amy Farha',
  avatar_url:
    'https://instagram.fsgn2-3.fna.fbcdn.net/v/t51.2885-15/e35/92356375_296667257994598_4014881559812578007_n.jpg?_nc_ht=instagram.fsgn2-3.fna.fbcdn.net&_nc_cat=110&_nc_ohc=6CA0fiMMu9oAX-yEkBf&oh=7ae7690637c9b699fb1c46fb2bc46d6c&oe=5EE2EC82',
  subtitle: 'Vice President',
});

const ListInforComponent = () => {
  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <View style={styles.screen1}>
          <ScrollView>
            {list.map((l, i) => (
              <ListItem
                key={i}
                leftAvatar={{source: {uri: l.avatar_url}}}
                title={l.name}
                subtitle={l.subtitle}
                bottomDivider
              />
            ))}
          </ScrollView>
        </View>
        <View style={styles.screen2}>
          <View style={styles.group1}>
            <Button
              buttonStyle={[styles.buttonAction]}
              icon={<Icon name="input" />}
            />
          </View>
          <View style={styles.group2}>
            <View style={styles.home}>
              <Button
                buttonStyle={[styles.buttonAction]}
                icon={<Icon name="home" />}
              />
            </View>
            <View style={styles.setting}>
              <Button
                buttonStyle={[styles.buttonAction]}
                icon={<Icon name="settings" />}
              />
            </View>
            <View style={styles.more}>
              <Button
                buttonStyle={[styles.buttonAction]}
                icon={<Icon name="more" />}
              />
            </View>
            <View style={styles.desktop}>
              <Button
                buttonStyle={[styles.buttonAction]}
                icon={<Icon name="computer" />}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  group1: {flex: 4, display: 'flex', alignItems: 'center', paddingTop: 10},
  group2: {
    flex: 6,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonAction: {
    borderRadius: 50,
    width: 60,
    height: 60,
    backgroundColor: '#F1F1F1',
  },
  body: {
    backgroundColor: '#FFFFFF',
    width: ScreenWidth - 30,
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: '#C8C8C9',
    display: 'flex',
    justifyContent: 'space-around',
    padding: 20,
    flex: 1,
    marginHorizontal: 10,
    marginBottom: 60,
    marginTop: 30,
    flexDirection: 'row',
  },
  screen1: {flex: 2},
  screen2: {flex: 1},
  text: {
    color: '#808080',
    fontWeight: 'bold',
    fontSize: 20,
  },
});

export default ListInforComponent;
