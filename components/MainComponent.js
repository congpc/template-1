import React, {useState} from 'react';
import {StyleSheet, View, Dimensions, Image, Text} from 'react-native';
import {Input, Button, Icon} from 'react-native-elements';

const ScreenWidth = Dimensions.get('window').width;
const MainComponent = () => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Input
          inputContainerStyle={[styles.txtInput, styles.text, styles.txtSearch]}
          rightIcon={{type: 'font-awesome', name: 'search', color: '#B0B0B0'}}
        />
      </View>
      <View style={styles.screen2}>
        <View style={styles.body}>
          <View style={styles.cardHeader}>
            <Text style={[styles.text, styles.textHeader]}>Alexander</Text>
            <Text style={[styles.text, styles.textDescription]}>
              Welcome to React Native
            </Text>
          </View>
          <View style={styles.cardBody}>
            <Button
              buttonStyle={[styles.txtInput]}
              title="Current booking"
              iconRight
              containerStyle={styles.buttonContainer}
              titleStyle={[styles.text, styles.titleButton]}
              iconContainerStyle={styles.iconButton}
              // icon={<Icon name="alarm" />}
            />
            <Button
              titleStyle={styles.text}
              buttonStyle={[styles.txtInput, styles.text]}
              title="Previous booking"
            />
            <Button
              titleStyle={styles.text}
              buttonStyle={[styles.txtInput, styles.text]}
              title="My profile"
            />
            <Button
              titleStyle={styles.text}
              buttonStyle={[styles.txtInput, styles.text]}
              title="Urgent ascitent"
            />
          </View>
        </View>
        <Image
          style={styles.tinyLogo}
          source={{
            uri:
              'https://kenh14cdn.com/2019/9/27/566226151661511044021668004432122225985389n-1569234596911848541502-1569517951952686128625.jpg',
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    height: 170,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  screen2: {
    flex: 5,
  },
  body: {
    backgroundColor: '#FFFFFF',
    width: ScreenWidth - 30,
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: '#C8C8C9',
    display: 'flex',
    justifyContent: 'space-around',
    padding: 20,
    height: 360,
  },
  cardHeader: {
    flex: 2,
    flexDirection: 'column',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardBody: {
    flex: 4,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  tinyLogo: {
    width: 90,
    height: 90,
    borderRadius: 50,
    position: 'absolute',
    top: -45,
    left: (ScreenWidth - 30) / 2 - 45,
    borderWidth: 4,
    borderColor: 'white',
  },
  txtInput: {
    height: 45,
    borderColor: '#EFF0F1',
    borderWidth: 3,
    borderRadius: 30,
    paddingHorizontal: 10,
    backgroundColor: '#F1F1F1',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: 10,
  },
  txtSearch: {
    width: ScreenWidth - 30,
    height: 50,
    marginTop: -50,
  },
  text: {
    color: '#808080',
    fontWeight: 'bold',
    fontSize: 20,
  },
  textHeader: {
    color: '#676767',
  },
  textDescription: {
    fontSize: 13,
    color: '#AEAEAE',
  },
});

export default MainComponent;
