import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TextInput,
  CheckBox,
  Image,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const ScreenWidth = Dimensions.get('window').width;
const LoginComponent = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [remember, setRemember] = useState(false);

  return (
    <View style={styles.loginScreen}>
      <View style={styles.header}>
        <Image
          style={styles.tinyLogo}
          source={{
            uri:
              'https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Spotify_logo_without_text.svg/1200px-Spotify_logo_without_text.svg.png',
          }}
        />
      </View>
      <View>
        <View style={styles.body}>
          <TextInput
            style={[styles.txtInput, styles.text]}
            onChangeText={text => setUsername(text)}
            value={username}
          />
          <TextInput
            style={[styles.txtInput, styles.txtPassword, styles.text]}
            onChangeText={text => setPassword(text)}
            value={password}
          />
          <View style={styles.checkboxContainer}>
            <Text style={[styles.label, styles.text]}>Remember me</Text>
            <CheckBox
              value={remember}
              onValueChange={setRemember}
              style={styles.checkbox}
            />
          </View>
        </View>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#ED7674', '#ED7F74', '#EF9578']}
          style={styles.btnLogin}>
          <Text style={[styles.text, styles.buttonText]}>Login</Text>
        </LinearGradient>
      </View>
      <View style={styles.footer} />
    </View>
  );
};

const styles = StyleSheet.create({
  loginScreen: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    flex: 1.8,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tinyLogo: {
    width: 60,
    height: 60,
    borderRadius: 30,
    borderWidth: 2,
    borderColor: 'white',
  },
  body: {
    backgroundColor: '#FFFFFF',
    width: ScreenWidth - 30,
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: '#C8C8C9',
    display: 'flex',
    justifyContent: 'space-around',
    padding: 30,
    height: 280,
  },
  footer: {
    flex: 2,
    backgroundColor: '#f4a324',
  },
  txtInput: {
    height: 45,
    borderColor: '#EFF0F1',
    borderWidth: 3,
    borderRadius: 30,
    paddingHorizontal: 10,
  },
  txtPassword: {
    marginTop: -20,
  },
  checkboxContainer: {
    flexDirection: 'row',
    marginBottom: 10,
    marginTop: -20,
    display: 'flex',
    justifyContent: 'flex-end',
  },
  checkbox: {
    alignSelf: 'center',
    width: 20,
    height: 20,
    borderColor: 'grey',
    borderWidth: 0.4,
  },
  label: {
    margin: 8,
  },
  btnLogin: {
    position: 'relative',
    top: -30,
    left: 90,
    width: 200,
    height: 60,
    backgroundColor: 'red',
    borderRadius: 30,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: '#FFFFFF',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
  },
  buttonText: {
    color: 'white',
  },
  text: {
    color: '#808080',
    fontWeight: 'bold',
    fontSize: 20,
  },
});

export default LoginComponent;
