import React, {useState} from 'react';
import {StyleSheet, View, Dimensions, Image} from 'react-native';
import MainComponent from './MainComponent';
import ListInforComponent from './ListInforComponent';
import {Input, Button, Icon} from 'react-native-elements';

const ScreenWidth = Dimensions.get('window').width;
const LayoutHomeComponent = () => {
  return (
    <View style={styles.container}>
      <View style={styles.body}>
        {/* <MainComponent /> */}
        <ListInforComponent />
      </View>
      <View style={styles.footer}>
        <View style={styles.tab1}>
          <Icon name="list" color="white" size={40} />
        </View>
        <View style={styles.tab2}>
          <Icon name="person" color="white" size={40} />
        </View>
        <View style={[styles.tab3, styles.activeTab]}>
          <Icon name="menu" color="white" size={40} />
        </View>
        <View style={styles.tab4}>
          <Icon name="notifications" color="white" size={40} />
        </View>
        <View style={styles.tab5}>
          <Icon name="contacts" color="white" size={40} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  body: {
    width: ScreenWidth,
    borderColor: '#C8C8C9',
    flex: 1,
  },
  footer: {
    height: 70,
    backgroundColor: '#252525',
    width: ScreenWidth,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  tinyLogo: {
    width: 30,
    height: 30,
  },
  activeTab: {
    backgroundColor: '#EE8F76',
    width: 70,
    height: 70,
    borderRadius: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -60,
  },
  text: {
    color: '#808080',
    fontWeight: 'bold',
    fontSize: 20,
  },
});

export default LayoutHomeComponent;
