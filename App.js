import React from 'react';
import {StyleSheet, ImageBackground} from 'react-native';
import LoginComponent from './components/LoginComponent';
import LayoutHomeComponent from './components/LayoutHomeComponent';

const image = {
  uri:
    'https://photo2.tinhte.vn/data/attachment-files/2017/11/4184892_C5ECF664-765F-4055-B25D-8F432DC66455.jpeg',
};

const App = () => {
  const a = require('./assets/images/wallpaper.jpg');
  console.log(a);
  return (
    <>
      <ImageBackground source={image} style={styles.background}>
        <LayoutHomeComponent />
        {/* <LoginComponent /> */}
      </ImageBackground>
    </>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'cover',
  },
});

export default App;
